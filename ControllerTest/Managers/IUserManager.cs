﻿using System.Collections.Generic;
using ControllerTest.Models;

namespace ControllerTest.Managers
{
    public interface IUserManager
    {
        int CreateUser(UserDTO createdUser);
        int UpdateUser(UserDTO updatedUser);
        int DeleteUser(int deletedUserId);
        UserDTO GetModelById(int userId);
        IEnumerable<UserDTO> GetModelsByStatus(bool deleted = false);
    }
}