﻿using System.Collections.Generic;
using AutoMapper;
using ControllerTest.Models;
using ControllerTest.Repositories;

namespace ControllerTest.Managers
{
    public class UserManager : IUserManager
    {
        private IUserRepository UserRepository { get; set; }

        public UserManager(IUserRepository userRepository)
        {
            this.UserRepository = userRepository;
        }

        public int CreateUser(UserDTO createdUser)
        {
            return this.UserRepository.CreateUser(Mapper.Map<User>(createdUser));
        }
        public int UpdateUser(UserDTO updatedUser)
        {
            return this.UserRepository.UpdateUser(Mapper.Map<User>(updatedUser));
        }
        public int DeleteUser(int deletedUserId)
        {
            return this.UserRepository.DeleteUser(deletedUserId);
        }
        public UserDTO GetModelById(int userId)
        {
            return Mapper.Map<UserDTO>(this.UserRepository.GetModelById(userId));
        }
        public IEnumerable<UserDTO> GetModelsByStatus(bool deleted = false)
        {
            return Mapper.Map<IEnumerable<UserDTO>>(this.UserRepository.GetModelsByStatus(deleted));
        }
    }
}
