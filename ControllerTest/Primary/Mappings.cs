﻿using AutoMapper;
using ControllerTest.Models;

namespace ControllerTest.Primary
{
    public static class Mappings
    {
        public static void Configure()
        {
            Mapper.Initialize(config => Initialize(config));
        }

        private static void Initialize(IMapperConfigurationExpression config)
        {
            MapUsers(config);
            MapComments(config);
        }

        private static void MapUsers(IMapperConfigurationExpression config)
        {
            config.CreateMap<User, UserDTO>();
            config.CreateMap<UserDTO, GetUserResponse>();

            config.CreateMap<CreateUserRequest, UserDTO>();
            config.CreateMap<UpdateUserRequest, UserDTO>();
            config.CreateMap<UserDTO, User>();
        }
        private static void MapComments(IMapperConfigurationExpression config)
        {
            config.CreateMap<Comment, CommentDTO>();
            config.CreateMap<CommentDTO, GetCommentResponse>();

            config.CreateMap<CommentRequest, CommentDTO>();
            config.CreateMap<CommentDTO, Comment>();
        }
    }
}
