﻿using System.Collections.Generic;
using AutoMapper;
using ControllerTest.Managers;
using ControllerTest.Models;
using Microsoft.AspNetCore.Mvc;

namespace ControllerTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserManager UserManager { get; set; }

        public UserController(IUserManager userManager)
        {
            this.UserManager = userManager;
        }

        // POST: api/User
        [HttpPost]
        public ActionResult<int> Post([FromBody] CreateUserRequest request)
        {
            return this.UserManager.CreateUser(Mapper.Map<UserDTO>(request));
        }
        // PUT: api/User/5
        [HttpPut]
        public ActionResult<int> Put([FromBody] UpdateUserRequest request)
        {
            return this.UserManager.UpdateUser(Mapper.Map<UserDTO>(request));
        }
        // DELETE: api/User/5
        [HttpDelete("{id}")]
        public ActionResult<int> Delete(int id)
        {
            return this.UserManager.DeleteUser(id);
        }
        // GET: api/User/5
        [HttpGet("{id}", Name = "Get")]
        public ActionResult<GetUserResponse> Get(int id)
        {
            return Mapper.Map<GetUserResponse>(this.UserManager.GetModelById(id));
        }
        // GET: api/User
        [HttpGet]
        public ActionResult<IEnumerable<GetUserResponse>> Get()
        {
            return Mapper.Map<List<GetUserResponse>>(this.UserManager.GetModelsByStatus());
        }
    }
}
