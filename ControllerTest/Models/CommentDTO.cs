﻿namespace ControllerTest.Models
{
    public class CommentDTO
    {
        public string Name { get; set; }
        public string Text { get; set; }
    }
}
