﻿namespace ControllerTest.Models
{
    public class GetCommentResponse
    {
        public string Name { get; set; }
        public string Text { get; set; }
    }
}
