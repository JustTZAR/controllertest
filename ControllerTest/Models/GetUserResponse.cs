﻿using System.Collections.Generic;

namespace ControllerTest.Models
{
    public class GetUserResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public int Age { get; set; }
        public bool IsActive { get; set; }
        public List<string> Tags { get; set; }
        public GetCommentResponse Comment { get; set; }
    }
}
