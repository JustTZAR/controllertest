﻿namespace ControllerTest.Models
{
    public class CommentRequest
    {
        public string Name { get; set; }
        public string Text { get; set; }
    }
}
