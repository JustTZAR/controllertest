﻿using System.Collections.Generic;

namespace ControllerTest.Models
{
    public class CreateUserRequest
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public int Age { get; set; }
        public bool IsActive { get; set; }
        public List<string> Tags { get; set; }
        public CommentRequest Comment { get; set; }
    }
}
