﻿using System.Collections.Generic;

namespace ControllerTest.Models
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public int Age { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public List<string> Tags { get; set; }
        public CommentDTO Comment { get; set; }
    }
}
