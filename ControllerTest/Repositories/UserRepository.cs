﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ControllerTest.Models;
using LiteDB;

namespace ControllerTest.Repositories
{
    public class UserRepository : IUserRepository
    {
        private string DbName { get; set; }

        public UserRepository()
        {
            this.DbName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "TestDb.db");
        }

        public int CreateUser(User createdUser)
        {
            using (LiteDatabase userContext = new LiteDatabase(this.DbName))
            {
                ILiteCollection<User> collection = userContext.GetCollection<User>("Users");
                collection.Insert(createdUser);
                return createdUser.Id;
            }
        }
        public int UpdateUser(User updatedUser)
        {
            using (LiteDatabase userContext = new LiteDatabase(this.DbName))
            {
                ILiteCollection<User> collection = userContext.GetCollection<User>("Users");
                collection.Update(updatedUser);
                return updatedUser.Id;
            }
        }
        public int DeleteUser(int deletedUserId)
        {
            using (LiteDatabase userContext = new LiteDatabase(this.DbName))
            {
                ILiteCollection<User> collection = userContext.GetCollection<User>("Users");
                User model = collection.FindById(deletedUserId);
                model.IsDeleted = true;
                collection.Update(model);
                return model.Id;
            }
        }
        public User GetModelById(int userId)
        {
            using (LiteDatabase userContext = new LiteDatabase(this.DbName))
            {
                ILiteCollection<User> collection = userContext.GetCollection<User>("Users");
                User model = collection.FindById(userId);
                return model;
            }
        }
        public IEnumerable<User> GetModelsByStatus(bool deleted = false)
        {
            using (LiteDatabase userContext = new LiteDatabase(this.DbName))
            {
                ILiteCollection<User> collection = userContext.GetCollection<User>("Users");
                IEnumerable<User> models = collection.Find(obj => obj.IsDeleted == deleted).ToList();
                return models;
            }
        }
    }
}
