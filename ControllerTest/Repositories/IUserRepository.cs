﻿using System.Collections.Generic;
using ControllerTest.Models;

namespace ControllerTest.Repositories
{
    public interface IUserRepository
    {
        int CreateUser(User createdUser);
        int UpdateUser(User updatedUser);
        int DeleteUser(int deletedUserId);
        User GetModelById(int userId);
        IEnumerable<User> GetModelsByStatus(bool deleted = false);
    }
}