﻿using System.Collections.Generic;
using System.Linq;
using ControllerTest.Controllers;
using ControllerTest.Managers;
using ControllerTest.Models;
using ControllerTest.Primary;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace ControllerTest.Tests
{
    public class UserControllerTests
    {
        public UserControllerTests()
        {
            Mappings.Configure();
        }

        [Fact]
        public void GetReturnsActionResultWithUsersList()
        {
            // Arrange
            Mock<IUserManager> mock = new Mock<IUserManager>();
            mock.Setup(manager => manager.GetModelsByStatus(false)).Returns(this.GetTestUsers());
            UserController controller = new UserController(mock.Object);

            // Act
            ActionResult<IEnumerable<GetUserResponse>> result = controller.Get();

            // Assert
            ActionResult<IEnumerable<GetUserResponse>> viewResult = Assert.IsType<ActionResult<IEnumerable<GetUserResponse>>>(result);
            IEnumerable<GetUserResponse> model = Assert.IsAssignableFrom<IEnumerable<GetUserResponse>>(viewResult.Value);
            Assert.Equal(this.GetTestUsers().Count(), model.Count());
        }
        [Fact]
        public void GetReturnsActionResultWithUserById()
        {
            // Arrange
            Mock<IUserManager> mock = new Mock<IUserManager>();
            mock.Setup(manager => manager.GetModelById(It.IsAny<int>())).Returns(this.GetTestUsers().FirstOrDefault());
            UserController controller = new UserController(mock.Object);

            // Act
            ActionResult<GetUserResponse> result = controller.Get(1);

            // Assert
            ActionResult<GetUserResponse> viewResult = Assert.IsType<ActionResult<GetUserResponse>>(result);
            GetUserResponse model = Assert.IsAssignableFrom<GetUserResponse>(viewResult.Value);
            Assert.NotNull(model);
            Assert.Equal(1, model.Id);
        }
        [Fact]
        public void PostReturnsActionResultUserId()
        {
            // Arrange
            CreateUserRequest request = new CreateUserRequest()
            {
                FirstName = "Alex",
                SecondName = "Melashchenko",
                Age = 23,
                IsActive = true,
                Tags = new List<string>()
                {
                    "cool",
                    "nice"
                },
                Comment = new CommentRequest()
                {
                    Name = "Vova",
                    Text = "text"
                }
            };
            Mock<IUserManager> mock = new Mock<IUserManager>();
            mock.Setup(manager => manager.CreateUser(It.IsAny<UserDTO>())).Returns(1);
            mock.Setup(manager => manager.GetModelById(It.IsAny<int>())).Returns(this.GetTestUsers().FirstOrDefault());
            UserController controller = new UserController(mock.Object);

            // Act
            ActionResult<int> createResult = controller.Post(request);
            ActionResult<GetUserResponse> getResult = controller.Get(1);

            // Assert
            ActionResult<int> createViewResult = Assert.IsType<ActionResult<int>>(createResult);
            int createModel = Assert.IsAssignableFrom<int>(createViewResult.Value);
            ActionResult<GetUserResponse> getViewResult = Assert.IsType<ActionResult<GetUserResponse>>(getResult);
            GetUserResponse getModel = Assert.IsAssignableFrom<GetUserResponse>(getViewResult.Value);
            Assert.Equal(1, createModel);
            Assert.Equal(request.FirstName, getModel.FirstName);
            Assert.Equal(request.SecondName, getModel.SecondName);
            Assert.Equal(request.Age, getModel.Age);
            Assert.Equal(request.IsActive, getModel.IsActive);
            Assert.Equal(request.Tags, getModel.Tags);
            Assert.Equal(request.Comment.Name, getModel.Comment.Name);
            Assert.Equal(request.Comment.Text, getModel.Comment.Text);
        }
        [Fact]
        public void PutReturnsActionResultUserId()
        {
            // Arrange
            UpdateUserRequest request = new UpdateUserRequest()
            {
                Id = 1,
                FirstName = "Alex",
                SecondName = "Melashchenko",
                Age = 23,
                IsActive = true,
                Tags = new List<string>()
                {
                    "cool",
                    "nice"
                },
                Comment = new CommentRequest()
                {
                    Name = "Vova",
                    Text = "text"
                }
            };
            Mock<IUserManager> mock = new Mock<IUserManager>();
            mock.Setup(manager => manager.UpdateUser(It.IsAny<UserDTO>())).Returns(1);
            mock.Setup(manager => manager.GetModelById(It.IsAny<int>())).Returns(this.GetTestUsers().FirstOrDefault());
            UserController controller = new UserController(mock.Object);

            // Act
            ActionResult<int> createResult = controller.Put(request);
            ActionResult<GetUserResponse> getResult = controller.Get(1);

            // Assert
            ActionResult<int> createViewResult = Assert.IsType<ActionResult<int>>(createResult);
            int createModel = Assert.IsAssignableFrom<int>(createViewResult.Value);
            ActionResult<GetUserResponse> getViewResult = Assert.IsType<ActionResult<GetUserResponse>>(getResult);
            GetUserResponse getModel = Assert.IsAssignableFrom<GetUserResponse>(getViewResult.Value);
            Assert.Equal(1, createModel);
            Assert.Equal(request.Id, getModel.Id);
            Assert.Equal(request.FirstName, getModel.FirstName);
            Assert.Equal(request.SecondName, getModel.SecondName);
            Assert.Equal(request.Age, getModel.Age);
            Assert.Equal(request.IsActive, getModel.IsActive);
            Assert.Equal(request.Tags, getModel.Tags);
            Assert.Equal(request.Comment.Name, getModel.Comment.Name);
            Assert.Equal(request.Comment.Text, getModel.Comment.Text);
        }
        [Fact]
        public void DeleteReturnsActionResultUserId()
        {
            // Arrange
            Mock<IUserManager> mock = new Mock<IUserManager>();
            mock.Setup(manager => manager.DeleteUser(It.IsAny<int>())).Returns(1);
            mock.Setup(manager => manager.GetModelById(It.IsAny<int>())).Returns((UserDTO)null);
            UserController controller = new UserController(mock.Object);

            // Act
            ActionResult<int> createResult = controller.Delete(1);
            ActionResult<GetUserResponse> getResult = controller.Get(1);

            // Assert
            ActionResult<int> createViewResult = Assert.IsType<ActionResult<int>>(createResult);
            int createModel = Assert.IsAssignableFrom<int>(createViewResult.Value);
            ActionResult<GetUserResponse> getViewResult = Assert.IsType<ActionResult<GetUserResponse>>(getResult);
            Assert.Equal(1, createModel);
            Assert.Null(getViewResult.Value);
        }

        private IEnumerable<UserDTO> GetTestUsers()
        {
            List<UserDTO> users = new List<UserDTO>
            {
                new UserDTO
                {
                    Id = 1,
                    FirstName = "Alex",
                    SecondName = "Melashchenko",
                    Age = 23,
                    IsActive = true,
                    Tags = new List<string>()
                    {
                        "cool",
                        "nice"
                    },
                    Comment = new CommentDTO()
                    {
                        Name = "Vova",
                        Text = "text"
                    }
                },
                new UserDTO
                {
                    Id = 2,
                    FirstName = "Max",
                    SecondName = "Chernata",
                    Age = 21,
                    IsActive = true,
                    Tags = new List<string>()
                    {
                        "normal",
                        "programmer"
                    },
                    Comment = new CommentDTO()
                    {
                        Name = "TestName",
                        Text = "CustomText"
                    }
                },
            };
            return users;
        }
    }
}
